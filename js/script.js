jQuery(document).ready(function($){
	
// выбранные пользователем места
let selectedPlaces = [];
// количество выбранных мест
let amountPl = document.getElementById("amountPlaces");
// общая стоимость выбранных мест
let pricePl = document.getElementById("totalPrice");

// цена одного места на сеанс
let priceForOnePlace = 0;
// id сеанса, на который выбираем места
let sessionID = -1;

// При загрузке страницы формируем список сеансов
$.get("getAllSessions.php", function(data, status){
	// Получаем все сенасы
	let sessions = JSON.parse(data);
	for(let i = 0; i < sessions.length; i++) {
		let sesDate = new Date(sessions[i].date);
		let dateNow = new Date();

		// Выбираем только те, который >= текущей даты 
		if(sesDate >= dateNow) {
			let session = "id: " + sessions[i].id + 
						" name: " + sessions[i].name +
						" date: " + sessions[i].date +
						" time: " + sessions[i].time + 
						" price: " + sessions[i].price_for_one_place;

			//формируем пункт списка сеансов
			session = `<option value="${sessions[i].id}" data-name="${sessions[i].name}" data-hall="${sessions[i].id_hall}" data-date="${sessions[i].date}" data-time="${sessions[i].time}" data-price="${sessions[i].price_for_one_place}">${session}</option>`;
			//добавляем его
			$(".sessions").append(session);
		}
	}
})
// после выполнения предыдущей функции
// *чтобы небыло непредсказуемости асинхронности*
.done(function() {
	// Получаем все сессии, которые отобразили в список
	let options = $("option");

	$.each(options, function( key, value ) {
		//вешаем обработчик нажатия на сессию
		value.addEventListener("click", function(){
			// Делаем "чистый зал" (нет забронированных и купленных мест)
			clearPlaces(".place.booked", "booked");
			clearPlaces(".place.bought", "bought");
			clearPlaces(".place.selected", "selected");

			priceForOnePlace = value.dataset.price;
			sessionID = value.value;

			$(".hall").css("visibility","visible");
			$(".selectedFilm").html(`${value.dataset.name} ${value.dataset.date} ${value.dataset.time}`)
		
			let data = {
				'id_sess': value.value
			};

			//вызываем сервис, который по id сеанса вернет все session_place
			// т.е. все забронированные и купленные места данного сеанса
			$.get("sessionPlacesByID.php", data, function(data, status){
				// может быть, что забронир/купленных мест
				// на сеанс нет
				if(data != "No data"){
					let blockPlaces = JSON.parse(data);

						for(let i = 0; i < blockPlaces.length; i++) {
							let tmp = blockPlaces[i];
							// находим это забронир/купленное место
							let place = $(`.place[data-row="${tmp.row}"][data-place="${tmp.place}"]`);
							// и "красим" его
							if(tmp.id_place_state == 2) {
								place[0].classList.add("booked");
							} else if(tmp.id_place_state == 3) {
								place[0].classList.add("bought");
							}
						}
					}
				}).done(function(){
					// Получаем все СВОБОДНЫЕ места
					let freePlaces = $(".place:not(.booked):not(.bought)");
					for(let i = 0; i < freePlaces.length; i++) {
						let tmp = freePlaces[i];
						tmp.addEventListener("click", function(){
							//это место было ранее не выбрано
							if(!this.classList.contains("selected")){
								// добавляем в массив выбранных мест
								selectedPlaces.push(this);

								let pl = `<li data-row="${this.dataset.row}" data-place="${this.dataset.place}">Ряд-${this.dataset.row}:Место-${this.dataset.place}</li>`;
								$(".selectedPlaces").append(pl);

								// обновляем данные о колич. выбранных мест
								// и их суммарной стоимости
								let curVal = +amountPl.dataset.val;
								amountPl.dataset.val = curVal + 1;
								amountPl.innerText = amountPl.dataset.val;

								let curPrice = +pricePl.dataset.val;
								pricePl.dataset.val = parseInt(curPrice) + parseInt(priceForOnePlace);
								pricePl.innerText = pricePl.dataset.val;
							} else {
								// это место уже является выбранным
								for( var i = 0; i < selectedPlaces.length; i++){ 
								   if ( selectedPlaces[i] === this) {
								   	// Удаляем из массивы выбранных мест
								     selectedPlaces.splice(i, 1); 
								   }
								}

								// Удаляем его из списки (справа от зала)
								$(`.selectedPlaces li[data-row="${this.dataset.row}"][data-place="${this.dataset.place}"]`).remove();

								// обновляем данные о колич. выбранных мест
								// и их суммарной стоимости
								let curVal = +amountPl.dataset.val;
								amountPl.dataset.val = curVal - 1;
								amountPl.innerText = amountPl.dataset.val;

								let curPrice = +pricePl.dataset.val;
								pricePl.dataset.val = parseInt(curPrice) - parseInt(priceForOnePlace);
								pricePl.innerText = pricePl.dataset.val;
							}
							// Если место было не выбрано, то выбираем его
							// Если выло выбрано, то отменяем выбор
							this.classList.toggle("selected");
						});
					}
				})
			})
		});
	});

// Обработка кнопки "Забронировать"
$("input[type='submit']").click(function(event){
	let telField = $("#telNumb").val();

	if(telField.trim().length == 0) {
		alert("Нужно заполнить телефон!");
		return false;
	}

	// массив с выбранными места готовим для отправки
	// на сервер
	let normArray = [];
	for(let i = 0; i < selectedPlaces.length; i++) {
		let tmp = {
			row: selectedPlaces[i].dataset.row,
			place: selectedPlaces[i].dataset.place
		}
		normArray.push(tmp)
	}

	let data_to_send = {
		phone: telField,
		places: normArray,
		session_id: sessionID
	};

	//отправляем на сервер
	$.get("reservatePlacesToSession.php", data_to_send, function(data){
		console.log(data);
	});
});

// функция для очистли элементов с данным selector
// от этого classToClear
function clearPlaces(selector, classToClear) {
	let need_clear_places = $(selector);

	for(let i = 0; i < need_clear_places.length; i++) {
		let tmp = need_clear_places[i];
		tmp.classList.remove(classToClear);
	}
}

})