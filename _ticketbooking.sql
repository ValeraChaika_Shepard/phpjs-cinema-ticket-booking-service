-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Ноя 18 2019 г., 15:09
-- Версия сервера: 10.3.13-MariaDB
-- Версия PHP: 7.1.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: ` ticketbooking`
--

-- --------------------------------------------------------

--
-- Структура таблицы `age_rating`
--

CREATE TABLE `age_rating` (
  `id` int(11) NOT NULL,
  `short_name` varchar(6) NOT NULL,
  `long_name` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `age_rating`
--

INSERT INTO `age_rating` (`id`, `short_name`, `long_name`) VALUES
(1, 'G', 'без ограничений'),
(2, 'M', 'все, но дети в сопр. взр.'),
(3, 'R', '16- в сопр. взросл'),
(4, 'X', 'если 17- то нельзя');

-- --------------------------------------------------------

--
-- Структура таблицы `film`
--

CREATE TABLE `film` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `duration` int(11) NOT NULL,
  `premiere_date` date NOT NULL,
  `id_age_rating` int(11) NOT NULL,
  `id_main_genre` int(11) NOT NULL,
  `description` text NOT NULL,
  `logo_link` varchar(255) NOT NULL,
  `trailer_link` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `film`
--

INSERT INTO `film` (`id`, `name`, `duration`, `premiere_date`, `id_age_rating`, `id_main_genre`, `description`, `logo_link`, `trailer_link`) VALUES
(1, 'Джокер', 121, '2019-10-03', 3, 2, 'Готэм, начало 1980-х годов. Комик Артур Флек живет с больной матерью, которая с детства учит его «ходить с улыбкой». Пытаясь нести в мир хорошее и дарить людям радость, Артур сталкивается с человеческой жестокостью и постепенно приходит к выводу, что этот мир получит от него не добрую улыбку, а ухмылку злодея Джокера.', 'https://st.kp.yandex.net/images/film_iphone/iphone360_1048334.jpg', 'https://youtu.be/50IJyz7ecqc'),
(2, 'Оно', 135, '2017-09-07', 3, 3, 'Небольшой городок в штате Мэн терроризирует загадочный серийный убийца, с нечеловеческой жестокостью умертвляющий детей. Тут и там находят разорванные тела, а иногда только их части. Семеро одиннадцатилетних ребят: Ричи Тозиер, Билл Денбро, Беверли Марш, Майк Хэнлон, Эдди Каспбрак, Бен Хэнском и Стэн Урис сталкиваются каждый по отдельности с загадочным злом — ужасающим монстром, способным принимать любые формы. В фильме Оно предстает в образе клоуна под названием Пеннивайз, заманивающего детей оранжевыми помпонами и воздушными шариками. В месте, известном как «Степи», ребята, объединившись в «Клуб Неудачников», решают найти и уничтожить чудовище. Лидером «Неудачников» становится Билл Денбро, брат которого был убит Пеннивайзом за год до этого, и теперь у Билла своя вендетта. Кроме монстра «Неудачников» преследует местный хулиган Генри Бауэрс со своими дружками, что, впрочем, ещё сильнее сплачивает друзей.', 'https://www.film.ru/sites/default/files/styles/thumb_260x400/public/movies/posters/16607995-946826.jpg', 'https://youtu.be/IisU-JHj_fU'),
(4, 'Титаник', 312, '1997-12-30', 1, 3, 'Иногда грустно(2', 'https://javarush.ru/groups/posts/645-stroki-v-java', 'https://javarush.ru/groups/posts/645-stroki-v-java');

-- --------------------------------------------------------

--
-- Структура таблицы `genre`
--

CREATE TABLE `genre` (
  `id` int(11) NOT NULL,
  `short_name` varchar(6) NOT NULL,
  `long_name` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `genre`
--

INSERT INTO `genre` (`id`, `short_name`, `long_name`) VALUES
(1, 'ком', 'комедия'),
(2, 'пф', 'приключенческий фильм'),
(3, 'уж', 'ужас'),
(4, 'дет', 'детектив');

-- --------------------------------------------------------

--
-- Структура таблицы `hall`
--

CREATE TABLE `hall` (
  `id` int(11) NOT NULL,
  `short_name` varchar(10) NOT NULL,
  `long_name` varchar(50) NOT NULL,
  `amount_rows` int(11) NOT NULL,
  `total_places` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `hall`
--

INSERT INTO `hall` (`id`, `short_name`, `long_name`, `amount_rows`, `total_places`) VALUES
(1, 'ос', 'основной', 8, 69);

-- --------------------------------------------------------

--
-- Структура таблицы `hall_row`
--

CREATE TABLE `hall_row` (
  `id` int(11) NOT NULL,
  `id_hall` int(11) NOT NULL,
  `row_number` int(11) NOT NULL,
  `amount_places_in_row` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `hall_row`
--

INSERT INTO `hall_row` (`id`, `id_hall`, `row_number`, `amount_places_in_row`) VALUES
(1, 1, 1, 6),
(2, 1, 2, 8),
(3, 1, 3, 9),
(4, 1, 4, 9),
(5, 1, 5, 9),
(6, 1, 6, 9),
(7, 1, 7, 9),
(8, 1, 8, 10);

-- --------------------------------------------------------

--
-- Структура таблицы `place_state`
--

CREATE TABLE `place_state` (
  `id` int(11) NOT NULL,
  `short_name` varchar(10) NOT NULL,
  `long_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `place_state`
--

INSERT INTO `place_state` (`id`, `short_name`, `long_name`) VALUES
(1, 'св', 'свободно'),
(2, 'зб', 'забронировано'),
(3, 'кп', 'выкуплено');

-- --------------------------------------------------------

--
-- Структура таблицы `reservation`
--

CREATE TABLE `reservation` (
  `id` int(11) NOT NULL,
  `id_session_place` int(11) NOT NULL,
  `phone_number` int(11) NOT NULL,
  `id_reservation_state` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `reservation`
--

INSERT INTO `reservation` (`id`, `id_session_place`, `phone_number`, `id_reservation_state`) VALUES
(1, 1, 713128969, 1),
(2, 2, 713128970, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `reservation_state`
--

CREATE TABLE `reservation_state` (
  `id` int(11) NOT NULL,
  `short_name` varchar(5) NOT NULL,
  `long_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `reservation_state`
--

INSERT INTO `reservation_state` (`id`, `short_name`, `long_name`) VALUES
(1, 'ож', 'в ожидании выкупа'),
(2, 'кп', 'выкуплено'),
(3, 'отм', 'отменена');

-- --------------------------------------------------------

--
-- Структура таблицы `session`
--

CREATE TABLE `session` (
  `id` int(11) NOT NULL,
  `id_film` int(11) NOT NULL,
  `id_hall` int(11) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `price_for_one_place` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `session`
--

INSERT INTO `session` (`id`, `id_film`, `id_hall`, `date`, `time`, `price_for_one_place`) VALUES
(1, 1, 1, '2019-11-20', '16:00:00', 200),
(2, 2, 1, '2019-11-21', '20:00:00', 150),
(3, 4, 1, '1997-03-04', '12:30:00', 250);

-- --------------------------------------------------------

--
-- Структура таблицы `session_place`
--

CREATE TABLE `session_place` (
  `id` int(11) NOT NULL,
  `id_session` int(11) NOT NULL,
  `row` int(11) NOT NULL,
  `place` int(11) NOT NULL,
  `id_place_state` int(11) NOT NULL,
  `phone_number` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `session_place`
--

INSERT INTO `session_place` (`id`, `id_session`, `row`, `place`, `id_place_state`, `phone_number`) VALUES
(1, 1, 4, 5, 2, '0713128877'),
(2, 1, 6, 8, 3, '0713128877'),
(3, 2, 1, 2, 3, '0713128877'),
(4, 2, 1, 3, 3, '0713128877'),
(5, 2, 8, 3, 2, '0713128877'),
(6, 2, 8, 4, 2, '0713128877'),
(7, 3, 1, 2, 2, '81'),
(8, 2, 4, 5, 2, '88005553535'),
(9, 2, 4, 6, 2, '88005553535'),
(10, 2, 4, 7, 2, '88005553535'),
(11, 1, 1, 3, 2, '123123123'),
(12, 1, 1, 4, 2, '123123123'),
(13, 2, 5, 7, 3, '909090'),
(14, 2, 5, 6, 3, '909090'),
(15, 2, 6, 6, 3, '909090'),
(16, 2, 6, 7, 3, '909090'),
(17, 2, 6, 2, 2, '335567'),
(18, 2, 6, 3, 2, '335567'),
(19, 1, 5, 3, 2, '12345'),
(20, 1, 5, 4, 2, '12345'),
(21, 1, 8, 1, 2, '123321123321'),
(22, 1, 8, 2, 2, '123321123321'),
(23, 1, 8, 3, 2, '123321123321'),
(24, 1, 8, 4, 2, '123321123321'),
(25, 2, 8, 8, 2, '77733339995'),
(26, 2, 8, 9, 2, '77733339995'),
(27, 2, 7, 8, 2, '77733339995'),
(28, 2, 7, 9, 2, '77733339995'),
(29, 2, 8, 10, 2, '77733339995'),
(30, 1, 3, 9, 2, '123123123123'),
(31, 1, 4, 9, 2, '123123123123'),
(32, 1, 4, 8, 2, '123123123123'),
(33, 1, 3, 8, 2, '123123123123'),
(34, 2, 3, 3, 2, '3334'),
(35, 2, 3, 4, 2, '3334');

-- --------------------------------------------------------

--
-- Структура таблицы `tmp_phones`
--

CREATE TABLE `tmp_phones` (
  `id` int(11) NOT NULL,
  `phone` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tmp_phones`
--

INSERT INTO `tmp_phones` (`id`, `phone`) VALUES
(1, '258-31-05'),
(2, '88005553535'),
(19, ''),
(20, ''),
(21, ''),
(22, 'ads'),
(23, 'cz');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `age_rating`
--
ALTER TABLE `age_rating`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `film`
--
ALTER TABLE `film`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_age_rating` (`id_age_rating`),
  ADD KEY `id_main_genre` (`id_main_genre`);

--
-- Индексы таблицы `genre`
--
ALTER TABLE `genre`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `hall`
--
ALTER TABLE `hall`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `hall_row`
--
ALTER TABLE `hall_row`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_hall` (`id_hall`);

--
-- Индексы таблицы `place_state`
--
ALTER TABLE `place_state`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `reservation`
--
ALTER TABLE `reservation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_session_place` (`id_session_place`,`id_reservation_state`),
  ADD KEY `id_reservation_state` (`id_reservation_state`);

--
-- Индексы таблицы `reservation_state`
--
ALTER TABLE `reservation_state`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `session`
--
ALTER TABLE `session`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_film` (`id_film`,`id_hall`),
  ADD KEY `id_hall` (`id_hall`);

--
-- Индексы таблицы `session_place`
--
ALTER TABLE `session_place`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_session` (`id_session`,`id_place_state`),
  ADD KEY `id_place_state` (`id_place_state`);

--
-- Индексы таблицы `tmp_phones`
--
ALTER TABLE `tmp_phones`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `age_rating`
--
ALTER TABLE `age_rating`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `film`
--
ALTER TABLE `film`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT для таблицы `genre`
--
ALTER TABLE `genre`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `hall`
--
ALTER TABLE `hall`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT для таблицы `hall_row`
--
ALTER TABLE `hall_row`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT для таблицы `place_state`
--
ALTER TABLE `place_state`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `reservation`
--
ALTER TABLE `reservation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `reservation_state`
--
ALTER TABLE `reservation_state`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `session`
--
ALTER TABLE `session`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `session_place`
--
ALTER TABLE `session_place`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT для таблицы `tmp_phones`
--
ALTER TABLE `tmp_phones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `film`
--
ALTER TABLE `film`
  ADD CONSTRAINT `film_ibfk_1` FOREIGN KEY (`id_age_rating`) REFERENCES `age_rating` (`id`),
  ADD CONSTRAINT `film_ibfk_2` FOREIGN KEY (`id_main_genre`) REFERENCES `genre` (`id`);

--
-- Ограничения внешнего ключа таблицы `hall_row`
--
ALTER TABLE `hall_row`
  ADD CONSTRAINT `hall_row_ibfk_1` FOREIGN KEY (`id_hall`) REFERENCES `hall` (`id`);

--
-- Ограничения внешнего ключа таблицы `reservation`
--
ALTER TABLE `reservation`
  ADD CONSTRAINT `reservation_ibfk_1` FOREIGN KEY (`id_reservation_state`) REFERENCES `reservation_state` (`id`),
  ADD CONSTRAINT `reservation_ibfk_2` FOREIGN KEY (`id_session_place`) REFERENCES `session_place` (`id`);

--
-- Ограничения внешнего ключа таблицы `session`
--
ALTER TABLE `session`
  ADD CONSTRAINT `session_ibfk_1` FOREIGN KEY (`id_film`) REFERENCES `film` (`id`),
  ADD CONSTRAINT `session_ibfk_2` FOREIGN KEY (`id_hall`) REFERENCES `hall` (`id`);

--
-- Ограничения внешнего ключа таблицы `session_place`
--
ALTER TABLE `session_place`
  ADD CONSTRAINT `session_place_ibfk_1` FOREIGN KEY (`id_place_state`) REFERENCES `place_state` (`id`),
  ADD CONSTRAINT `session_place_ibfk_2` FOREIGN KEY (`id_session`) REFERENCES `session` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
