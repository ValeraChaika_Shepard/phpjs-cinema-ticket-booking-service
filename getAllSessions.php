<?php
	require 'connectdb.php';
	
	// Делаем запрос
	$sql = "SELECT session.id as id, id_hall, id_film, name, id_hall, date, time, price_for_one_place FROM session INNER JOIN film ON session.id_film = film.id;";

	// Получаем результат запроса
	$result = mysqli_query($conn, $sql);

	// Создаем массив, который вернем пользователю
	$rows = array();

	if(mysqli_num_rows($result) > 0) {
		while($r = mysqli_fetch_assoc($result)) {
			array_push($rows, $r);
		}
		print json_encode($rows, JSON_UNESCAPED_SLASHES |JSON_UNESCAPED_UNICODE);
	} else {
		echo "No data";
	}
	mysqli_close($conn);
?>